# schemas

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`schemas` exports a set of object factories for use with a hypercore. Factories build payloads for a cobox-log using JSON schemas for validation and Protocol Buffers for encoding.

## Install
```
npm i -g @coboxcoop/schemas
```

## Usage
This module also exports all currently used message schemas in CoBox as raw validators and encoders.

See `/test` for examples

In order to use the protocol-buffers message encoders, you'll need to pre-compile them.

```
npm run compile
```

## API
See swagger documentation... (we won't have this for a while).

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
