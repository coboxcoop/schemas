const definitions = require('../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['author', 'timestamp', 'type', 'version'],
  properties: {
    type: {
      type: 'string',
      pattern: '^command/announce'
    },
    version: {
      type: 'string',
      pattern: '^1.0.0$'
    },
    timestamp: { type: 'integer' },
    author: { $ref: '#/definitions/peerId' },
  },
  definitions
}
