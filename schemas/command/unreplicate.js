const definitions = require('../../definitions')

// orders seeder to stop replicating given address

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['type', 'timestamp', 'author', 'content', 'version'],
  properties: {
    type: {
      type: 'string',
      pattern: '^command/unreplicate'
    },
    version: {
      type: 'string',
      pattern: '^1.0.0$'
    },
    timestamp: { type: 'integer' },
    author: { $ref: '#/definitions/peerId' },
    content: {
      type :'object',
      required: ['name', 'address'],
      properties: {
        name: { type: 'string' },
        address: { $ref: '#/definitions/feedId' },
      }
    }
  },
  definitions
}
