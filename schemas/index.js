module.exports = {
  command: {
    replicate: require('./command/replicate'),
    unreplicate: require('./command/unreplicate'),
    announce: require('./command/announce'),
    hide: require('./command/hide')
  },
  peer: {
    about: require('./peer/about'),
    connection: require('./peer/connection')
  },
  space: {
    about: require('./space/about')
  },
  seeders: {
    broadcast: require('./seeders/broadcast'),
    confirm: require('./seeders/confirm')
  }
}
