const definitions = require('../../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['type', 'timestamp', 'content', 'version'],
  properties: {
    type: {
      type: 'string',
      pattern: '^peer/connection'
    },
    version: {
      type: 'string',
      pattern: '^1.0.0$'
    },
    timestamp: { type: 'integer' },
    content: {
      type: 'object',
      properties: {
        connected: { type: 'boolean' },
        peer: { $ref: '#/definitions/peerId' }
      }
    }
  },
  definitions
}
