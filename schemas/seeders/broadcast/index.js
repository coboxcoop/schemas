const definitions = require('../../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['type', 'timestamp', 'author', 'content', 'version'],
  properties: {
    type: {
      type: 'string',
      pattern: '^seeders/broadcast'
    },
    version: {
      type: 'string',
      pattern: '^1.0.0$'
    },
    timestamp: { type: 'integer' },
    author: { $ref: '#/definitions/peerId' },
    content: {
      type: 'object',
      required: ['address', 'encryptionKey'],
      properties: {
        address: { $ref: '#/definitions/feedId' },
        encryptionKey: { $ref: '#/definitions/feedId' }
      }
    }
  },
  definitions
}
