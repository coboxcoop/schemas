const definitions = require('../../../definitions')

module.exports = {
  $schema: 'http://json-schema.org/schema#',
  type: 'object',
  required: ['type', 'author', 'timestamp', 'content'],
  properties: {
    type: {
      type: 'string',
      pattern: '^space/about'
    },
    version: {
      type: 'string',
      pattern: '^1.0.0$'
    },
    author: { $ref: '#/definitions/peerId' },
    timestamp: { type: 'integer' },
    content: {
      type: 'object',
      oneof: [{
        properties: {
          name: { type: 'string' }
        }
      }]
    }
  },
  definitions
}
