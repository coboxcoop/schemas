const { describe } = require('tape-plus')
const crypto = require('crypto')

const { encodings, validators } = require('../../')
const Hide = encodings.command.hide
const isHide = validators.command.hide
const author = crypto.randomBytes(32).toString('hex')

describe('Hide: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      type: "command/hide"
    }
    assert.ok(Hide.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author
    }
    assert.throws(() => Hide.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      type: "command/hide"
    }
    assert.throws(() => Hide.encode(msg), 'Error: spaceId is required', 'throws when missing spaceId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      version: "1.0.0",
      type: "command/hide"
    }
    assert.throws(() => Hide.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })
})

describe('Hide: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      type: "command/hide"
    }
    assert.ok(isHide(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author
    }
    assert.notOk(isHide(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      version: "1.0.0",
      type: "command/hide"
    }
    assert.notOk(isHide(msg), 'invalid without timestamp')
    next()
  })
})

