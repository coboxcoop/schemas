const { describe } = require('tape-plus')
const crypto = require('crypto')

const { encodings, validators } = require('../../')
const Replicate = encodings.command.replicate
const isReplicate = validators.command.replicate
const address = crypto.randomBytes(32).toString('hex')
const author = crypto.randomBytes(32).toString('hex')
const name = 'magma'

describe('Replicate: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      type: "command/replicate",
      content: { name, address }
    }
    assert.ok(Replicate.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      content: { name, address }
    }
    assert.throws(() => Replicate.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      type: "command/replicate",
      version: "1.0.0",
      content: { name, address }
    }
    assert.throws(() => Replicate.encode(msg), 'Error: spaceId is required', 'throws when missing spaceId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "command/replicate",
      version: "1.0.0",
      content: { name, address }
    }
    assert.throws(() => Replicate.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })
})

describe('Replicate: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "command/replicate",
      version: "1.0.0",
      content: { name, address }
    }
    assert.ok(isReplicate(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      content: { name, address }
    }
    assert.notOk(isReplicate(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      version: "1.0.0",
      type: "command/replicate",
      content: { name, address }
    }
    assert.notOk(isReplicate(msg), 'invalid without timestamp')
    next()
  })
})

