const { describe } = require('tape-plus')
const crypto = require('crypto')

const { encodings, validators } = require('../../')
const Unreplicate = encodings.command.unreplicate
const isUnreplicate = validators.command.unreplicate
const author = crypto.randomBytes(32).toString('hex')
const address = crypto.randomBytes(32).toString('hex')
const name = 'magma'

describe('Unreplicate: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      type: "command/unreplicate",
      content: { name, address }
    }
    assert.ok(Unreplicate.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      content: { name, address }
    }
    assert.throws(() => Unreplicate.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      type: "command/unreplicate",
      content: { name, address }
    }
    assert.throws(() => Unreplicate.encode(msg), 'Error: spaceId is required', 'throws when missing spaceId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "command/unreplicate",
      version: "1.0.0",
      content: { name, address }
    }
    assert.throws(() => Unreplicate.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })
})

describe('Unreplicate: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      version: "1.0.0",
      type: "command/unreplicate",
      content: { name, address }
    }
    assert.ok(isUnreplicate(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      version: "1.0.0",
      content: { name, address }
    }
    assert.notOk(isUnreplicate(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "command/unreplicate",
      version: "1.0.0",
      content: { name, address }
    }
    assert.notOk(isUnreplicate(msg), 'invalid without timestamp')
    next()
  })
})

