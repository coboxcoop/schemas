const { describe } = require('tape-plus')
const Factory = require('../../')
const crypto = require('crypto')

const { encodings, validators } = Factory
const About = encodings.peer.about
const isAbout = validators.peer.about
const author = crypto.randomBytes(32).toString('hex')

describe('PeerAbout: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      version: "1.0.0",
      timestamp: 1580584726389,
      author,
      type: "peer/about",
      content: {
        name: "magma"
      }
    }
    assert.ok(About.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      content: {
        name: "magma"
      }
    }
    assert.throws(() => About.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      version: "1.0.0",
      timestamp: 1580584726389,
      type: "peer/about",
      content: {
        name: "magma"
      }
    }
    assert.throws(() => About.encode(msg), 'Error: peerId is required', 'throws when missing peerId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      version: "1.0.0",
      author,
      type: "peer/about",
      content: {
        name: "magma"
      }
    }
    assert.throws(() => About.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      type: "peer/about",
      author
    }
    assert.throws(() => About.encode(msg), 'Error: content is required', 'throws when missing content')
    next()
  })
})

describe('PeerAbout: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      version: "1.0.0",
      type: "peer/about",
      content: {
        name: "magma"
      }
    }
    assert.ok(isAbout(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      version: "1.0.0",
      content: {
        name: "magma"
      }
    }
    assert.notOk(isAbout(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      version: "1.0.0",
      author,
      type: "peer/about",
      content: {
        name: "magma"
      }
    }
    assert.notOk(isAbout(msg), 'invalid without timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author
    }
    assert.notOk(isAbout(msg), 'invalid without content')
    next()
  })
})

