const { describe } = require('tape-plus')
const crypto = require('crypto')
const { encodings, validators } = require('../../')
const Connection = encodings.peer.connection
const isConnection = validators.peer.connection

describe('PeerConnection: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      type: "peer/connection",
      content: {
        connected: true
      }
    }
    assert.ok(Connection.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = { timestamp: 1580584726389,
      version: "1.0.0",
      content: {
        connected: false
      }
    }
    assert.throws(() => Connection.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      version: "1.0.0",
      type: "peer/connection",
      content: {
        name: "magma"
      }
    }
    assert.throws(() => Connection.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      type: "peer/connection"
    }
    assert.throws(() => Connection.encode(msg), 'Error: content is required', 'throws when missing content')
    next()
  })
})

describe('PeerConnection: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      type: "peer/connection",
      version: "1.0.0",
      content: {
        connected: true
      }
    }
    assert.ok(isConnection(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      content: {
        connected: true
      }
    }
    assert.notOk(isConnection(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      version: "1.0.0",
      type: "peer/connection",
      content: {
        connected: true
      }
    }
    assert.notOk(isConnection(msg), 'invalid without timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      content: {
        connected: true
      }
    }
    assert.notOk(isConnection(msg), 'invalid without content')
    next()
  })
})

