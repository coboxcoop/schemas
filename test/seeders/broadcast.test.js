const { describe } = require('tape-plus')
const Factory = require('../../')
const crypto = require('crypto')

const { encodings, validators } = Factory
const Broadcast = encodings.seeders.broadcast
const isBroadcast = validators.seeders.broadcast
const author = crypto.randomBytes(32).toString('hex')

describe('SeedersBroadcast: encodings', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      type: "seeders/broadcast",
      version: "1.0.0",
      timestamp: 1580584726389,
      author,
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.ok(Broadcast.encode(msg), 'encodes the message')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      version: "1.0.0",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.throws(() => Broadcast.encode(msg), 'Error: type is required', 'throws when missing type')
    next()
  })

  context('invalid without author', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      type: "seeders/broadcast",
      version: "1.0.0",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.throws(() => Broadcast.encode(msg), 'Error: spaceId is required', 'throws when missing spaceId')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "seeders/broadcast",
      version: "1.0.0",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.throws(() => Broadcast.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      type: "seeders/broadcast"
    }
    assert.throws(() => Broadcast.encode(msg), 'Error: timestamp is required', 'throws when missing timestamp')
    next()
  })
})

describe('SeedersBroadcast: validators', (context) => {
  context('valid', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      type: "seeders/broadcast",
      version: "1.0.0",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.ok(isBroadcast(msg), 'valid')
    next()
  })

  context('invalid without type', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      author,
      version: "1.0.0",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.notOk(isBroadcast(msg), 'invalid without type')
    next()
  })

  context('invalid without timestamp', (assert, next) => {
    let msg = {
      author,
      type: "seeders/broadcast",
      version: "1.0.0",
      content: {
        address: crypto.randomBytes(32).toString('hex'),
        encryptionKey: crypto.randomBytes(32).toString('hex'),
        broadcasting: true
      }
    }
    assert.notOk(isBroadcast(msg), 'invalid without timestamp')
    next()
  })

  context('invalid without content', (assert, next) => {
    let msg = {
      timestamp: 1580584726389,
      version: "1.0.0",
      author,
      type: "seeders/broadcast"
    }
    assert.notOk(isBroadcast(msg), 'invalid without timestamp')
    next()
  })
})

